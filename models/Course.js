/*
name
description
slots
price
isActive
enrollees
*/
const mongoose=require("mongoose");	


const courseSchema = new mongoose.Schema({
	name: {
		type:String,
		require:[true,"Course name is required"]
	}, 
	description: {
		type:String,
		require:[true,,"Description is required"]
	}, 
	price: {
		type:Number,
		require:[true,,"Price is required"]
	}, 
	slots: {
		type:Number,
		require:[true,,"Slots is required"]
	},
	isActive: {
		type:Boolean,
		default:true
	},
	createOn: {
		type:Date,
		default:new Date()
	}, 
	enrollees: [
		{
			userId:{
				type:String,
				require:[true,,"UserId is required"]
			},
			enrolledOn: {
				type:Date,
				default:new Date()
			} 
		}
	 ]
});
module.exports=mongoose.model("Course", courseSchema);



