/*
  - firstName - String
    - lastName - String
    - email -String
    - password - String
    - isAdmin - String
    - mobileNo - String
    - createOn-date
    - enrollments - Array of objects
        - courseId - String
        - enrolledOn - Date (Default value - new Date object)
        - status - String (Default value - Enrolled)

*/
const mongoose=require("mongoose");	

const userSchema = new mongoose.Schema({
	firstName: {
		type:String,
		require:[true,"firstName is required"]
	}, 
	lastName: {
		type:String,
		require:[true,"lastName is required"]
	}, 
	email: {
		type:String,
		require:[true,"email is required"]
	}, 
	password: {
		type:String,
		require:[true,"password is required"]
	},
	isAdmin: {
		type:Boolean,
		default:false
	},
	mobileNo: {
		type:String,
		require:[true,"password is required"]
	}, 
	createOn: {
		type:Date,
		default:new Date()
	}, 
	enrollments: [
		{
			courseId:{
				type:String,
				require:[true,"courseId is required"]
			},
			enrolledOn: {
				type:Date,
				default:new Date()
			},
			status: {
				type:String,
				default:"Enrolled"
			} 
		}
	 ]
})
module.exports=mongoose.model("User", userSchema);

