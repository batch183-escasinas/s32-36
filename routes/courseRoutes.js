const express=require("express");
const router=express.Router();
const courseControllers=require("../controllers/courseControllers");
const auth=require("../auth");


router.post("/",auth.verify,(req,res)=>{
	const courseData=auth.decode(req.headers.authorization);
	if(courseData.isAdmin){
		courseControllers.addCourse(courseData).then(resultFromController=>res.send(resultFromController));
	}else{
		res.send("You dont have permission");
	}
})

router.get("/all",auth.verify,(req,res)=>{
	courseControllers.getAllCourses().then(resultFromController=>res.send(resultFromController));
})
router.get("/",(req,res)=>{

	courseControllers.getAllActive().then(resultFromController=>res.send(resultFromController));
})

router.get("/:courseId", (req,res)=>{
	courseControllers.getCourse(req.params.courseId).then(resultFromController=>res.send(resultFromController));
})
router.put("/:courseId",auth.verify,(req,res)=>{
	courseControllers.updateCourse(req.params.courseId,req.body).then(resultFromController=>res.send(resultFromController));
})

router.patch("/:courseId/archive",auth.verify,(req,res)=>{
	courseControllers.archiveCourse(req.params.courseId,req.body).then(resultFromController=>res.send(resultFromController));
})

module.exports=router;

