const express=require("express");
const router=express.Router();
const userControllers=require("../controllers/userControllers");
const auth=require("../auth");

router.get("/checkEmail",(req,res)=>{
	userControllers.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
})

router.post("/register",(req,res)=>{
	userControllers.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
})
router.post("/login",(req,res)=>{
	userControllers.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
})
router.post("/details",(req,res)=>{
	userControllers.getProfile(req.body).then(resultFromController=>res.send(resultFromController));
})


router.get("/details",auth.verify,(req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	console.log(userData);
	userControllers.getProfile(userData).then(resultFromController=>res.send(resultFromController));
})



router.post("/enroll",auth.verify,(req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	let data={
		userId:userData.id,
		courseId:req.body.courseId
	}
	if(userData.isAdmin){
		res.send("You're not allowed to access this pages");
			
	}else{
		userControllers.enroll(data).then(resultFromController=>res.send(resultFromController));
	}
})

module.exports=router;
