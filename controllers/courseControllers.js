const Course=require("../models/Course");


module.exports.addCourse=(reqBody)=>{
let newCourse= new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	})
	return 	newCourse.save().then((course,error)=>{
		if(error){
			return false;
		}else{
			console.log
			return true;
		}
	});
}

module.exports.getAllCourses=()=>{
	return Course.find({}).then(result=>result);
}
module.exports.getAllActive=()=>{
	return Course.find({isActive:true}).then(result=>result);
}


module.exports.getCourse=(courseId)=>{

return Course.findById(courseId).then((getCourse,errorlog)=>{
	if(errorlog){
		console.log(errorlog);
		return false;
	}else{
		return getCourse;
	}
})
}
module.exports.updateCourse=(courseId,reqBody)=>{
let updateCourse={
	name:reqBody.name,
	description:reqBody.description,
	price:reqBody.price,
	slots:reqBody.slots
}
return Course.findByIdAndUpdate(courseId,updateCourse).then((courseUpdate,error)=>{
	if(error){
		return false;
	}else{
		return true;
	}
})
}

module.exports.archiveCourse=(courseId,reqBody)=>{

// return Course.findById(courseId).then((result,error)=>{
// 	if(error){
// 		return false;
// 	}else{
// 		result.isActive=reqBody.isActive;
// 		result.save();
// 		return true;
// 	}
// })


let updateArchiveField={
	isActive:false
}
return Course.findByIdAndUpdate(courseId,updateArchiveField).then((updateArchive,error)=>{
	if(error){
		return false;
	}else{
		return true;
	}
})
}

